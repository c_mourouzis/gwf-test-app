package com.chrismour.gwftestapp.activities.login;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.chrismour.gwftestapp.R;
import com.chrismour.gwftestapp.activities.main.MeterListActivity;
import com.chrismour.gwftestapp.httpclient.APIClientLogin;
import com.chrismour.gwftestapp.httpclient.APIInterfaceLogin;
import com.chrismour.gwftestapp.httpclient.NetworkConnectionInterceptor;
import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.chrismour.gwftestapp.models.tokens.LoginModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    //GWF API Interface
    private APIInterfaceLogin apiInterface;

    //LoginModel-Login Response
    private LoginModel loginModel;

    //Shared Preferences were the tokens will be stored internally
    private SharedPreferences sharedPreferences=null;
    private SharedPreferences.Editor editor;

    // UI Initializations
    private Button btnLogin;
    private TextView txtEmail, txtPassword;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //make it full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar

        setContentView(R.layout.activity_login);

        //Initialize LoginModel
        loginModel = LoginModel.getInstance();

        //InitializeShared Preferences
        sharedPreferences = getSharedPreferences(getResources()
                .getString(R.string.sharedPreferences_token), Context.MODE_PRIVATE);
        editor = getSharedPreferences(getResources()
                .getString(R.string.sharedPreferences_token), Context.MODE_PRIVATE).edit();


        //If already loggedIn (access already stored) go directly to dashboard activity
        if (sharedPreferences.getString(gwfTestAppConstants.accessTag, null)!=null) {
            Intent intentLogin = new Intent(this, MeterListActivity.class);
            startActivity(intentLogin);
        }
        //First Login or was Logout
        else {
            //Initialize GWF API interface
            apiInterface = APIClientLogin.getClient(getApplicationContext()).create(APIInterfaceLogin.class);

            //TextViews and buttons
            btnLogin = (Button) findViewById(R.id.btnLogin);
            txtEmail = (TextView) findViewById(R.id.txtEmail);
            txtPassword = (TextView) findViewById(R.id.txtPassword);

            //When login button is clicked
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //If fields are empty, user cannot continue
                    if (txtEmail.getText().toString().isEmpty() || txtPassword.getText().toString().isEmpty())
                        Toast.makeText(LoginActivity.this, gwfTestAppConstants.emtpyLoginDetails, Toast.LENGTH_LONG).show();
                    else {
                        //Initialize Login Call
                        Call<LoginModel> callUserLogin = apiInterface.login(txtEmail.getText().toString(), txtPassword.getText().toString());

                        // Set up progress before call
                        final ProgressDialog loading;
                        loading = new ProgressDialog(LoginActivity.this);
                        loading.setMax(100);
                        loading.setMessage(gwfTestAppConstants.loadingMessage);
                        loading.setTitle(gwfTestAppConstants.loadingTitle);
                        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        // show it
                        loading.show();

                        //Call the api
                        callUserLogin.enqueue(new Callback<LoginModel>() {
                            @Override
                            public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
                                //If successfull store the refresh and access token and go to Dashboard
                                if (response.isSuccessful()) {
                                    loading.dismiss();
                                    assert response.body() != null;

                                    //Retrofit serializes the response directly to LoginModel class using Gson
                                    loginModel = response.body();

                                    //Store tokens internally
                                    editor.putString(gwfTestAppConstants.refreshTag, loginModel.getRefresh());
                                    editor.putString(gwfTestAppConstants.accessTag, loginModel.getAccess());
                                    editor.commit();

                                    //Go to Dashboard Activity
                                    Intent intentLogin = new Intent(LoginActivity.this, MeterListActivity.class);
                                    startActivity(intentLogin);

                                } else {
                                    //If call unsuccessful pop-pup the error message - error response from server
                                    try {
                                        loading.dismiss();
                                        if (response.errorBody() != null) {
                                            String errorMessage;
                                            String errorTitle = null;
                                            //Log.e("test", response.errorBody().string());
                                            try {
                                                JsonObject error = JsonParser.parseString(response.errorBody().string()).getAsJsonObject();
                                                errorTitle = error.get(gwfTestAppConstants.errorResponseTitleTag).getAsString();
                                                errorMessage = error.get(gwfTestAppConstants.errorResponseMessageTag).getAsString();
                                            } catch (JsonParseException e) {
                                                errorMessage = response.errorBody().string();
                                            } catch (NullPointerException err) {
                                                errorMessage = gwfTestAppConstants.callErrorMessage;
                                            }

                                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

                                            builder.setTitle(errorTitle)
                                                    .setMessage(errorMessage);

                                            builder.setPositiveButton(gwfTestAppConstants.dialogOk, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    // User clicked OK button
                                                }
                                            });

                                            // Create the AlertDialog
                                            AlertDialog dialog = builder.create();

                                            dialog.show();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            //If call fails check if the error is No Network or anything else and pop-up the error
                            @Override
                            public void onFailure(@NonNull Call<LoginModel> call, @NonNull Throwable t) {
                                loading.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                if (t instanceof NetworkConnectionInterceptor.NoConnectivityException) {
                                    builder.setTitle(gwfTestAppConstants.noNetworkTitle)
                                            .setMessage(gwfTestAppConstants.noNetworkMessage);

                                    builder.setPositiveButton(gwfTestAppConstants.dialogOk, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User clicked OK button
                                        }
                                    });
                                } else {
                                    builder.setTitle(gwfTestAppConstants.callErrorTitle)
                                            .setMessage(gwfTestAppConstants.callErrorMessage);

                                    builder.setPositiveButton(gwfTestAppConstants.dialogOk, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User clicked OK button
                                        }
                                    });
                                }
                                // Create the AlertDialog
                                AlertDialog dialog = builder.create();

                                dialog.show();
                                call.cancel();
                            }

                        });
                    }
                }
            });
        }
    }

    //If back button is pressed exit app
    public void onBackPressed()
    {
        finishAffinity();
    }
}
