package com.chrismour.gwftestapp.activities.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.chrismour.gwftestapp.R;
import com.chrismour.gwftestapp.adapters.StateAdapter;
import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.chrismour.gwftestapp.models.readouts.MetersModel;
import com.chrismour.gwftestapp.models.readouts.State;
import com.chrismour.gwftestapp.models.readouts.StateList;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * A fragment representing a single Meter detail screen.
 * This fragment is either contained in a {@link MeterListActivity}
 * in two-pane mode (on tablets) or a {@link MeterDetailActivity}
 * on handsets.
 */
public class MeterDetailFragment extends Fragment implements OnMapReadyCallback {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    //Initialize Meter model
    private MetersModel mItem;

    //Initialize GoogleMap
    private GoogleMap mMap;

    //Initialize State adapter and list view
    private StateAdapter stateAdapter;
    private ListView lvState;

    //Initialize UI components
    private TextView txtVolume, txtBatteryLifetime, txtLastReadout, txtMeterTypeDetail, txtMeterSerialDetail;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MeterDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //If call
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            Gson gson = new Gson();
            //Get json string and convert it to MeterModel class
            mItem = gson.fromJson(getArguments().getString(gwfTestAppConstants.itemJsonTag), MetersModel.class);

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(getArguments().getString(ARG_ITEM_ID));

            txtMeterTypeDetail = (TextView) activity.findViewById(R.id.txtMeterTypeDetail);
            txtMeterSerialDetail = (TextView) activity.findViewById(R.id.txtMeterTypeSerialDetail);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.meter_detail, container, false);

        //Initialize MapView
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        mapFragment.getMapAsync(this);

        //Initialize ListView
        lvState = (ListView) rootView.findViewById(R.id.lvState);

        //Since the screen is scrolling, enable scrolling only in ListView when touched there
        lvState.setOnTouchListener(new ListView.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        //Initialize UI componentns
        txtVolume = (TextView) rootView.findViewById(R.id.txtVolume);
        txtBatteryLifetime = (TextView) rootView.findViewById(R.id.txtBatteryLifetime);
        txtLastReadout = (TextView) rootView.findViewById(R.id.txtLastReadout);


        //Set Volume and Battery Life
        txtVolume.setText("Volume: " + mItem.getVolume()+ " m3");
        txtBatteryLifetime.setText("Battery Lifetime: " + String.valueOf(mItem.getBatteryLifetime()));

        //Set Serial and Type
        txtMeterTypeDetail.setText(mItem.getCommModType());
        txtMeterSerialDetail.setText(mItem.getCommModSerial());

        //Set Last Readout
        if (mItem.getLastEntry()==null)
            txtLastReadout.setText("Last Readout: -");
        else {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df_input = new SimpleDateFormat(gwfTestAppConstants.ISO_8601_extendedZ);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df_output = new SimpleDateFormat(gwfTestAppConstants.textDateFormat);
            Date parsed = null;
            String outputDate = "";
            try {
                parsed = df_input.parse(mItem.getLastEntry().toString());
                outputDate = df_output.format(parsed);
                txtLastReadout.setText("Last Readout: " + outputDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //Convert State class to an array of key values to be set in the ListView adapter. Dynamically show only the State values for each meter
        Gson gson = new Gson();
        String myJsonString = gson.toJson(mItem.getState(), State.class);

        JsonObject myJsonObject = gson.fromJson(myJsonString, JsonObject.class);

        ArrayList<StateList> stateArrayList = new ArrayList<>();

        //Get Keys and Values of State Object
        Map<String, Object> attributes = new HashMap<String, Object>();
        Set<Map.Entry<String, JsonElement>> entrySet = myJsonObject.entrySet();
        for(Map.Entry<String,JsonElement> entry : entrySet){
            StateList stateList = new StateList();
            stateList.setKey(entry.getKey());
            stateList.setValue(myJsonObject.get(entry.getKey()).getAsBoolean());
            stateArrayList.add(stateList);
        }

        //Set State Adapter
        stateAdapter = new StateAdapter(getContext(), stateArrayList);
        lvState.setAdapter(stateAdapter);

        return rootView;
    }

    //Show Map
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        //Disable MapScrolling and zooming
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabledDuringRotateOrZoom(false);

        //If Meter has location details show a Marker
        if (mItem.getLat()!=null && mItem.getLng()!=null) {
            LatLng loc = new LatLng(mItem.getLat(), mItem.getLng());
            mMap.addMarker(new MarkerOptions()
                    .position(loc)
                    .title("Serial Number: "+mItem.getCommModSerial())
                    .snippet("Measurement Point: " +mItem.getMpName())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            //mMap.animateCamera( CameraUpdateFactory.zoomTo( 20.0f ) );

        }
    }
}