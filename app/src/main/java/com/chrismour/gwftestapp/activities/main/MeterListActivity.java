package com.chrismour.gwftestapp.activities.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.chrismour.gwftestapp.R;
import com.chrismour.gwftestapp.activities.login.LoginActivity;
import com.chrismour.gwftestapp.httpclient.APIClient;
import com.chrismour.gwftestapp.httpclient.APIInterface;
import com.chrismour.gwftestapp.httpclient.NetworkConnectionInterceptor;
import com.chrismour.gwftestapp.httpclient.TokenServiceHolder;
import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.chrismour.gwftestapp.models.readouts.MetersModel;
import com.chrismour.gwftestapp.models.readouts.TotalValuesModel;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An activity representing a list of Meters. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link MeterDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class MeterListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    // Models
    private TotalValuesModel totalValuesModel;
    private List<MetersModel> metersModels, filteredDataList;

    //UI components
    private TextView txtTotalUsage, txtTotalMeters, txtTotalReadouts, txtTotalAlerts;
    private ImageView imgLogout;
    SearchView searchView;

    //GWF API Interface
    private APIInterface apiInterface;

    //RecyclerView Adapters
    private View recyclerView;
    private MetersRecyclerViewAdapter metersRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meter_list);

        //Initialize toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        //For tablets
        if (findViewById(R.id.meter_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        //Initialization of models
        totalValuesModel = TotalValuesModel.getInstance();
        metersModels = new ArrayList<>();
        filteredDataList = new ArrayList<>();

        //Initialize recycler view
        recyclerView = findViewById(R.id.meter_list);

        //Search View
        searchView = (SearchView)   findViewById(R.id.svSearch);
        searchView.setIconified(false);
        searchView.clearFocus();

        //When text changes in search view filter the shown meters by their id
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                //Every text change filters the data based on Meter Id and sets the recycler view adapter
                filteredDataList = filter(metersModels, s);
                metersRecyclerViewAdapter.setFilter(filteredDataList);
                return true;
            }
        });

        //Initialize GWF API interface
        TokenServiceHolder tokenServiceHolder = TokenServiceHolder.getInstance(); //TokenService Holds the API interface used for refreshing the authorization
        apiInterface = APIClient.getClient(getApplicationContext()).create(APIInterface.class); //Initialize the API interface
        tokenServiceHolder.set(apiInterface); //set the service

        //Read the current access token from internal memory to use it for authorizing the calls
        SharedPreferences settings = getSharedPreferences(getResources()
                .getString(R.string.sharedPreferences_token), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences(getResources()
                .getString(R.string.sharedPreferences_token), Context.MODE_PRIVATE).edit();
        String accessToken = settings.getString(gwfTestAppConstants.accessTag, null);

        //If logout remove the tokens from the internal memory and go back to login screen
        imgLogout = (ImageView) findViewById(R.id.imageView);
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MeterListActivity.this);
                builder.setTitle("Logout?");
                builder.setMessage("Are you sure that you want to logout?");
                builder.setCancelable(true);

                builder.setPositiveButton(
                        "Yes",
                        (dialog, id) ->
                        {
                            editor.putString(gwfTestAppConstants.accessTag, null);
                            editor.putString(gwfTestAppConstants.refreshTag, null);
                            editor.commit();
                            Intent intentLogin = new Intent(MeterListActivity.this, LoginActivity.class);
                            startActivity(intentLogin);
                        });

                builder.setNegativeButton(
                        "No",
                        (dialog, id) -> dialog.cancel());

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        //Initialize TextViews
        txtTotalUsage = (TextView) findViewById(R.id.txtTotalUsage);
        txtTotalMeters = (TextView) findViewById(R.id.txtTotalMeters);
        txtTotalReadouts = (TextView) findViewById(R.id.txtTotalReadouts);
        txtTotalAlerts = (TextView) findViewById(R.id.txtTotalAlerts);

        //Initialize the total values call
        Call<TotalValuesModel> callTotalValues = apiInterface.getTotalValues(gwfTestAppConstants.bearerString + accessToken);

        // Set up progress before call
        final ProgressDialog loading;
        loading = new ProgressDialog(MeterListActivity.this);
        loading.setMax(100);
        loading.setMessage(gwfTestAppConstants.loadingMessage);
        loading.setTitle(gwfTestAppConstants.loadingTitle);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        loading.show();

        //call the api
        callTotalValues.enqueue(new Callback<TotalValuesModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<TotalValuesModel> call, @NonNull Response<TotalValuesModel> response) {
                //If successful show the total values data and call the meters API
                if (response.isSuccessful()) {
                    //loading.dismiss();
                    assert response.body() != null;
                    totalValuesModel = response.body();

                    //Show the data
                    txtTotalUsage.setText(totalValuesModel.getVolume().toString());
                    txtTotalMeters.setText(totalValuesModel.getMeters().toString());
                    txtTotalReadouts.setText(totalValuesModel.getReadouts().toString());
                    txtTotalAlerts.setText(totalValuesModel.getErrors().toString());

                    //Read the access token again (if was refreshed, new would be stored before this call)
                    SharedPreferences settings = getSharedPreferences(getResources()
                            .getString(R.string.sharedPreferences_token), Context.MODE_PRIVATE);
                    String accessToken = settings.getString(gwfTestAppConstants.accessTag, null);

                    //Initialize meters call
                    Call<List<MetersModel>> callTotalMeters = apiInterface.getMeters(gwfTestAppConstants.bearerString + accessToken);

                    //call the api
                    callTotalMeters.enqueue(new Callback<List<MetersModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<MetersModel>> call, @NonNull Response<List<MetersModel>> response) {

                            //If successful show the data
                            if (response.isSuccessful()) {
                                loading.dismiss();
                                assert response.body() != null;

                                //Response is a list of MeterModel. This list is shown in a recycler view
                                metersModels = response.body();
                                assert recyclerView != null;
                                setupRecyclerView((RecyclerView) recyclerView, metersModels);
                            }
                        }
                        //If call fails check if the error is No Network or anything else and pop-up the error
                        @Override
                        public void onFailure(@NonNull Call<List<MetersModel>> call, @NonNull Throwable t) {
                            loading.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(MeterListActivity.this);
                            if (t instanceof NetworkConnectionInterceptor.NoConnectivityException) {
                                builder.setTitle(gwfTestAppConstants.noNetworkTitle)
                                        .setMessage(gwfTestAppConstants.noNetworkMessage);

                                builder.setPositiveButton(gwfTestAppConstants.dialogOk, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User clicked OK button
                                    }
                                });
                            } else {
                                builder.setTitle(gwfTestAppConstants.callErrorTitle)
                                        .setMessage(gwfTestAppConstants.callErrorMessage);

                                builder.setPositiveButton(gwfTestAppConstants.dialogOk, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User clicked OK button
                                    }
                                });
                            }
                            // Create the AlertDialog
                            AlertDialog dialog = builder.create();

                            dialog.show();
                            call.cancel();
                        }

                    });


                }
            }
            //If call fails check if the error is No Network or anything else and pop-up the error
            @Override
            public void onFailure(@NonNull Call<TotalValuesModel> call, @NonNull Throwable t) {
                loading.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(MeterListActivity.this);
                if (t instanceof NetworkConnectionInterceptor.NoConnectivityException) {
                    builder.setTitle(gwfTestAppConstants.noNetworkTitle)
                            .setMessage(gwfTestAppConstants.noNetworkMessage);

                    builder.setPositiveButton(gwfTestAppConstants.dialogOk, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                        }
                    });
                } else {
                    builder.setTitle(gwfTestAppConstants.callErrorTitle)
                            .setMessage(gwfTestAppConstants.callErrorMessage);

                    builder.setPositiveButton(gwfTestAppConstants.dialogOk, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                        }
                    });
                }
                // Create the AlertDialog
                AlertDialog dialog = builder.create();

                dialog.show();
                call.cancel();
            }

        });
    }

    //Set up the Recycler View
    private void setupRecyclerView(@NonNull RecyclerView recyclerView, List<MetersModel> metersModels) {
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        metersRecyclerViewAdapter = new MetersRecyclerViewAdapter(this, metersModels, mTwoPane);
        recyclerView.setAdapter(metersRecyclerViewAdapter);
    }

    //RecyclerView adapter responsible for showing the meters data
    public static class MetersRecyclerViewAdapter
            extends RecyclerView.Adapter<MetersRecyclerViewAdapter.ViewHolder> {

        private final MeterListActivity mParentActivity;
        private List<MetersModel> metersModels;
        private final boolean mTwoPane;

        //When an item(meter) is clicked
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onClick(View view) {
                //Get MeterModel of clicked item
               MetersModel item = (MetersModel) view.getTag();

               //In case the data are empty
               if (item == null) {
                   Toast.makeText(mParentActivity,"No Data Available! Please logout and login again!", Toast.LENGTH_LONG);
               }
               //Meter is valid
               else {
                   Gson gson = new Gson();
                   //Convert item class to json string to easily transfer it to MeterDetail Activity (will be converted back to class in the next activity)
                   String itemJson = gson.toJson(item, MetersModel.class);
                   //If tablet
                   if (mTwoPane) {
                       Bundle arguments = new Bundle();
                       arguments.putString(MeterDetailFragment.ARG_ITEM_ID, item.getMeterId());
                       arguments.putString(gwfTestAppConstants.itemJsonTag, itemJson);
                       MeterDetailFragment fragment = new MeterDetailFragment();
                       fragment.setArguments(arguments);
                       mParentActivity.getSupportFragmentManager().beginTransaction()
                               .replace(R.id.meter_detail_container, fragment)
                               .commit();
                   }
                   //If mobile
                   else {
                       Context context = view.getContext();
                       Intent intent = new Intent(context, MeterDetailActivity.class);
                       //send the meterid to be shown as a title
                       intent.putExtra(MeterDetailFragment.ARG_ITEM_ID, item.getMeterId());
                       //send the whole class as json string
                       intent.putExtra(gwfTestAppConstants.itemJsonTag, itemJson);

                       context.startActivity(intent);
                   }
               }
            }
        };

        //Initialize Adapter
        MetersRecyclerViewAdapter(MeterListActivity parent,
                                  List<MetersModel> metersModels, boolean mTwoPane) {
            this.metersModels = metersModels;
            this.mTwoPane = mTwoPane;
            mParentActivity = parent;
        }

        @NotNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.meter_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mMeterId.setText(metersModels.get(position).getMeterId());
            holder.mMeterType.setText(metersModels.get(position).getCommModType());
            holder.mMeterSerial.setText(metersModels.get(position).getCommModSerial());
            /*if ((metersModels.get(position).getLat()==null) || (metersModels.get(position).getLat() == null))
                holder.mMarker.setVisibility(View.GONE);*/

            holder.itemView.setTag(metersModels.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return metersModels.size();
        }

        //Filtered data list in adapter
        void setFilter(List<MetersModel> FilteredDataList) {
            this.metersModels = FilteredDataList;
            notifyDataSetChanged();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mMeterId;
            final TextView mMeterType;
            final TextView mMeterSerial;
            //final ImageView mMarker;

            ViewHolder(View view) {
                super(view);
                mMeterId = (TextView) view.findViewById(R.id.txtMeterId);
                mMeterType = (TextView) view.findViewById(R.id.txtMeterType);
                mMeterSerial = (TextView) view.findViewById(R.id.txtMeterSerial);
                //mMarker = (ImageView) view.findViewById(R.id.imgMarker);
            }
        }
    }

    //Function that implements the search view filtering based on meter id. It returnes a filtered data list to be set in the recycler view adapter
    private List<MetersModel> filter(List<MetersModel> dataList, String newText) {
        newText=newText.toLowerCase();
        String text;
        filteredDataList=new ArrayList<>();
        for(MetersModel dataFromDataList:dataList){
            text=dataFromDataList.getMeterId();

            if(text.contains(newText)){
                filteredDataList.add(dataFromDataList);
            }
        }

        return filteredDataList;
    }

    //If back button is pressed exit app
    public void onBackPressed()
    {
        finishAffinity();
    }
}