package com.chrismour.gwftestapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chrismour.gwftestapp.R;
import com.chrismour.gwftestapp.models.readouts.StateList;

import java.util.ArrayList;

//ListView adapter thta shows the State data of each meter
public class StateAdapter extends BaseAdapter {
    private Context context;
    public static ArrayList<StateList> state;

    //Initialize Adapter
    public StateAdapter(Context context, ArrayList<StateList> state) {
        this.context = context;
        this.state = state;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getCount() {
        return state.size();
    }

    @Override
    public Object getItem(int position) {
        return state.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    //Show Data
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.state_list_content, null, true);

            holder.mState = (TextView) convertView.findViewById(R.id.txtState);
            holder.mStateValue = (TextView) convertView.findViewById(R.id.txtStateValue);

            //Pretty show each error description
            String stateKey;
            switch (state.get(position).getKey()){
                case "backflow":
                    stateKey = "Backflow";
                    break;
                case "battery_low":
                    stateKey = "Battery Low";
                    break;
                case "broken_pipe":
                    stateKey = "Broken Pipe";
                    break;
                case "communication_error":
                    stateKey = "Communication Error";
                    break;
                case "continuous_flow":
                    stateKey = "Continuous Flow";
                    break;
                case "encoder_error":
                    stateKey = "Encoder Error";
                    break;
                case "parsing_error":
                    stateKey = "Parsing Error";
                    break;
                case "system_error":
                    stateKey = "System Error";
                    break;
                case "t_air_error":
                    stateKey = "T Air Error";
                    break;
                case "t_water_error":
                    stateKey = "T Water Error";
                    break;
                case "us_water_level":
                    stateKey = "Us Water Level";
                    break;
                case "w_air_error":
                    stateKey = "W Air Error";
                    break;
                case "v_sensor_comm_timout":
                    stateKey = "V Sensor Comm Timout";
                    break;
                case "w_water_error":
                    stateKey = "W Water Error";
                    break;
                case "velocity_error":
                    stateKey = "Velocity Error";
                    break;
                case "water_level_error":
                    stateKey = "Water Level Error";
                    break;
                default:
                    stateKey = "Unknown Error";
            }

            //Show State Values
            holder.mState.setText(stateKey);
            holder.mStateValue.setText(state.get(position).getValue().toString());

            if (state.get(position).getValue())
                holder.mStateValue.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }
        return convertView;
    }

    static class ViewHolder {
        TextView mState;
        TextView mStateValue;
    }
}
