package com.chrismour.gwftestapp.models.readouts;

//Model class that holds the key values of State model when shown in MetersDetail screen
public class StateList {
    public StateList() {
    }

    String key;
    Boolean value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }


}
