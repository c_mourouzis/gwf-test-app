package com.chrismour.gwftestapp.models.tokens;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//Model class that holds the token when retrieved during login
public class LoginModel {
    @SerializedName("refresh")
    @Expose
    private String refresh;
    @SerializedName("access")
    @Expose
    private String access;

    private static volatile LoginModel instance=null;

    private LoginModel() {
    }

    public static synchronized  LoginModel getInstance() {
        if(instance == null) {
            instance = new LoginModel();
        }
        return instance;
    }

    public void setInstance(){
        instance=LoginModel.this;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }



}