package com.chrismour.gwftestapp.models.readouts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//ModelClass that holds the State data
public class State {
    @SerializedName("continuous_flow")
    @Expose
    private Boolean continuousFlow;
    @SerializedName("broken_pipe")
    @Expose
    private Boolean brokenPipe;
    @SerializedName("battery_low")
    @Expose
    private Boolean batteryLow;
    @SerializedName("backflow")
    @Expose
    private Boolean backflow;
    @SerializedName("communication_error")
    @Expose
    private Boolean communicationError;
    @SerializedName("parsing_error")
    @Expose
    private Boolean parsingError;
    @SerializedName("encoder_error")
    @Expose
    private Boolean encoderError;
    @SerializedName("us_water_level")
    @Expose
    private Boolean usWaterLevel;
    @SerializedName("v_sensor_comm_timout")
    @Expose
    private Boolean vSensorCommTimout;
    @SerializedName("water_level_error")
    @Expose
    private Boolean waterLevelError;
    @SerializedName("t_air_error")
    @Expose
    private Boolean tAirError;
    @SerializedName("t_water_error")
    @Expose
    private Boolean tWaterError;
    @SerializedName("w_air_error")
    @Expose
    private Boolean wAirError;
    @SerializedName("w_water_error")
    @Expose
    private Boolean wWaterError;
    @SerializedName("velocity_error")
    @Expose
    private Boolean velocityError;
    @SerializedName("system_error")
    @Expose
    private Boolean systemError;

    public Boolean getContinuousFlow() {
        return continuousFlow;
    }

    public void setContinuousFlow(Boolean continuousFlow) {
        this.continuousFlow = continuousFlow;
    }

    public Boolean getBrokenPipe() {
        return brokenPipe;
    }

    public void setBrokenPipe(Boolean brokenPipe) {
        this.brokenPipe = brokenPipe;
    }

    public Boolean getBatteryLow() {
        return batteryLow;
    }

    public void setBatteryLow(Boolean batteryLow) {
        this.batteryLow = batteryLow;
    }

    public Boolean getBackflow() {
        return backflow;
    }

    public void setBackflow(Boolean backflow) {
        this.backflow = backflow;
    }

    public Boolean getCommunicationError() {
        return communicationError;
    }

    public void setCommunicationError(Boolean communicationError) {
        this.communicationError = communicationError;
    }

    public Boolean getParsingError() {
        return parsingError;
    }

    public void setParsingError(Boolean parsingError) {
        this.parsingError = parsingError;
    }

    public Boolean getEncoderError() {
        return encoderError;
    }

    public void setEncoderError(Boolean encoderError) {
        this.encoderError = encoderError;
    }

    public Boolean getUsWaterLevel() {
        return usWaterLevel;
    }

    public void setUsWaterLevel(Boolean usWaterLevel) {
        this.usWaterLevel = usWaterLevel;
    }

    public Boolean getvSensorCommTimout() {
        return vSensorCommTimout;
    }

    public void setvSensorCommTimout(Boolean vSensorCommTimout) {
        this.vSensorCommTimout = vSensorCommTimout;
    }

    public Boolean getWaterLevelError() {
        return waterLevelError;
    }

    public void setWaterLevelError(Boolean waterLevelError) {
        this.waterLevelError = waterLevelError;
    }

    public Boolean gettAirError() {
        return tAirError;
    }

    public void settAirError(Boolean tAirError) {
        this.tAirError = tAirError;
    }

    public Boolean gettWaterError() {
        return tWaterError;
    }

    public void settWaterError(Boolean tWaterError) {
        this.tWaterError = tWaterError;
    }

    public Boolean getwAirError() {
        return wAirError;
    }

    public void setwAirError(Boolean wAirError) {
        this.wAirError = wAirError;
    }

    public Boolean getwWaterError() {
        return wWaterError;
    }

    public void setwWaterError(Boolean wWaterError) {
        this.wWaterError = wWaterError;
    }

    public Boolean getVelocityError() {
        return velocityError;
    }

    public void setVelocityError(Boolean velocityError) {
        this.velocityError = velocityError;
    }

    public Boolean getSystemError() {
        return systemError;
    }

    public void setSystemError(Boolean systemError) {
        this.systemError = systemError;
    }
}

