package com.chrismour.gwftestapp.models.readouts;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//Model class that holds the Meters response
public class MetersModel{

    @SerializedName("lat")
    @Expose
    private Float lat;
    @SerializedName("lng")
    @Expose
    private Float lng;
    @SerializedName("mp_name")
    @Expose
    private String mpName;
    @SerializedName("meter_id")
    @Expose
    private String meterId;
    @SerializedName("comm_mod_type")
    @Expose
    private String commModType;
    @SerializedName("comm_mod_serial")
    @Expose
    private String commModSerial;
    @SerializedName("last_entry")
    @Expose
    private Object lastEntry;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("battery_lifetime")
    @Expose
    private Integer batteryLifetime;
    @SerializedName("state")
    @Expose
    private State state;

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public String getMpName() {
        return mpName;
    }

    public void setMpName(String mpName) {
        this.mpName = mpName;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public String getCommModType() {
        return commModType;
    }

    public void setCommModType(String commModType) {
        this.commModType = commModType;
    }

    public String getCommModSerial() {
        return commModSerial;
    }

    public void setCommModSerial(String commModSerial) {
        this.commModSerial = commModSerial;
    }

    public Object getLastEntry() {
        return lastEntry;
    }

    public void setLastEntry(Object lastEntry) {
        this.lastEntry = lastEntry;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public Integer getBatteryLifetime() {
        return batteryLifetime;
    }

    public void setBatteryLifetime(Integer batteryLifetime) {
        this.batteryLifetime = batteryLifetime;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

}
