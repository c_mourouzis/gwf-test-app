package com.chrismour.gwftestapp.models.readouts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


//Model class that holds the Total Values response data
public class TotalValuesModel {
    @SerializedName("meters")
    @Expose
    private Integer meters;
    @SerializedName("volume")
    @Expose
    private Float volume;
    @SerializedName("errors")
    @Expose
    private Integer errors;
    @SerializedName("readouts")
    @Expose
    private Integer readouts;

    private static volatile TotalValuesModel instance=null;

    private TotalValuesModel() {
    }

    public static synchronized TotalValuesModel getInstance() {
        if(instance == null) {
            instance = new TotalValuesModel();
        }
        return instance;
    }

    public void setInstance(){
        instance= TotalValuesModel.this;
    }

    public Integer getMeters() {
        return meters;
    }

    public void setMeters(Integer meters) {
        this.meters = meters;
    }

    public Float getVolume() {
        return volume;
    }

    public void setVolume(Float volume) {
        this.volume = volume;
    }

    public Integer getErrors() {
        return errors;
    }

    public void setErrors(Integer errors) {
        this.errors = errors;
    }

    public Integer getReadouts() {
        return readouts;
    }

    public void setReadouts(Integer readouts) {
        this.readouts = readouts;
    }
}