package com.chrismour.gwftestapp.models.constants;


//Class that holds all constants of the application
public class gwfTestAppConstants {
    //Contact
    public static final String gwfContactUs = "https://gwf.ch/kontakt/";

    // API Client
    public static final String baseUrl = "https://test-api.gwf.ch";
    public static final String contentTypeForm = "Content-Type:application/x-www-form-urlencoded";

    //GWF APis
    public static final String postGwfLogin = "/auth/token/";
    public static final String postGwfRefreshToken = "/auth/token/refresh/";
    public static final String getAllValues = "/reports/measurements/total/";
    public static final String getAllMeters = "/reports/measurements/";

    //Authorization
    public static final String bearerString= "Bearer ";

    //Loading
    public static final String loadingTitle = "Loading";
    public static final String loadingMessage = "Please wait...";

    //Errors
    public static final String emtpyLoginDetails = "Please fill all the fields!";
    public static final String noNetworkTitle = "No Internet Connection!";
    public static final String noNetworkMessage = "Network Failure! Please check your internet connection!";
    public static final String callErrorTitle = "Error!";
    public static final String callErrorMessage = "Something went wrong! Please try again!";
    public static final String errorResponseTitleTag= "description";
    public static final String errorResponseMessageTag= "detail";

    //Dialog
    public static final String dialogOk = "OK";
    public static final String dialogCancel = "Cancel";

    //Tags
    public static final String refreshTag = "refresh";
    public static final String accessTag = "access";
    public static final String itemJsonTag = "itemJson";

    //Titles
    public static final String dashboardActivityTitle = "Reports";

    //Date Formats
    public static final String ISO_8601_extendedZ = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String textDateFormat = "dd/MM/yyyy HH:mm:ss";

}
