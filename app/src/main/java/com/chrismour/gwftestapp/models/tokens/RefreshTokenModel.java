package com.chrismour.gwftestapp.models.tokens;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//Model class that gets the refresh token
public class RefreshTokenModel {
    @SerializedName("access")
    @Expose
    private String access;

    private static volatile RefreshTokenModel instance=null;

    private RefreshTokenModel() {
    }

    public static synchronized RefreshTokenModel getInstance() {
        if(instance == null) {
            instance = new RefreshTokenModel();
        }
        return instance;
    }

    public void setInstance(){
        instance= RefreshTokenModel.this;
    }


    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }
}