package com.chrismour.gwftestapp.httpclient;

import androidx.annotation.Nullable;

//Singleton TokenServiceHolder class responsible to hold the API interface that is used by the TokenAuthenticator
public class TokenServiceHolder {

    private static volatile TokenServiceHolder instance=null;

    private TokenServiceHolder() {
    }

    public static synchronized  TokenServiceHolder getInstance() {
        if(instance == null) {
            instance = new TokenServiceHolder();
        }
        return instance;
    }

    public void setInstance(){
        instance=TokenServiceHolder.this;
    }

    APIInterface tokenService = null;

    @Nullable
    public APIInterface get() {
        return tokenService;
    }

    public void set(APIInterface tokenService) {
        this.tokenService = tokenService;
    }
}
