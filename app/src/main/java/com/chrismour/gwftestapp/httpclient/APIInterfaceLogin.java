package com.chrismour.gwftestapp.httpclient;


import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.chrismour.gwftestapp.models.tokens.LoginModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

//API Interface for login call
public interface APIInterfaceLogin {
    /* GWF APIs */
    /* Login */
    @Headers({gwfTestAppConstants.contentTypeForm })
    @POST(gwfTestAppConstants.postGwfLogin)
    @FormUrlEncoded
    Call<LoginModel> login(@Field("email") String email, @Field("password") String password);
}
