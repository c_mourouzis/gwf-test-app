package com.chrismour.gwftestapp.httpclient;

import android.content.Context;
import android.content.SharedPreferences;

import com.chrismour.gwftestapp.R;
import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.chrismour.gwftestapp.models.tokens.RefreshTokenModel;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

//Authenticator calls that is responsible for handling expired access token and refresh it
public class TokenAuthenticator implements Authenticator {
    private Context context;
    private final TokenServiceHolder tokenServiceHolder;
    private RefreshTokenModel refreshTokenModel;

    //Initialize Authenticator
    public TokenAuthenticator(Context context, TokenServiceHolder tokenServiceHolder) {
        this.context = context;
        this.tokenServiceHolder = tokenServiceHolder;
    }

    //If an API returns 401 then this function is called automatically to call the refresh token API
    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        if (tokenServiceHolder == null) {
            return null;
        }

        //Get the last access token and refresh token from internal memory
        SharedPreferences settings = context.getSharedPreferences(context.getResources()
                .getString(R.string.sharedPreferences_token), context.MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences(context.getResources()
                .getString(R.string.sharedPreferences_token), context.MODE_PRIVATE).edit();
        String access = settings.getString(gwfTestAppConstants.accessTag, null);
        String refresh = settings.getString(gwfTestAppConstants.refreshTag, null);

        //Synchronous refresh token call
        retrofit2.Response<RefreshTokenModel> retrofitResponse = tokenServiceHolder.get().refreshToken(access, refresh).execute();

        //If response is successfull get the new token store it internally and re-call the API that the access was expired before
        if (retrofitResponse != null) {

            //Get the new access token
            refreshTokenModel = retrofitResponse.body();

            //Store the new access token
            editor.putString(gwfTestAppConstants.accessTag, refreshTokenModel.getAccess());
            editor.commit();

            //Call again the previous call that the access was expired
            return response.request().newBuilder()
                    .header("Authorization", gwfTestAppConstants.bearerString+refreshTokenModel.getAccess())
                    .build();
        }

        return null;
    }
}
