package com.chrismour.gwftestapp.httpclient;


import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.chrismour.gwftestapp.models.readouts.MetersModel;
import com.chrismour.gwftestapp.models.readouts.TotalValuesModel;
import com.chrismour.gwftestapp.models.tokens.RefreshTokenModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

//API Interface for calls that need authorization token
public interface APIInterface {
    /* Refresh token*/
    @Headers({gwfTestAppConstants.contentTypeForm })
    @POST(gwfTestAppConstants.postGwfRefreshToken)
    @FormUrlEncoded
    Call<RefreshTokenModel> refreshToken(@Header("Authorization") String token, @Field("refresh") String refreshToken);

    /* Get Total Values*/
    @GET(gwfTestAppConstants.getAllValues)
    Call<TotalValuesModel> getTotalValues(@Header("Authorization") String token);

    /* Get Meters*/
    @GET(gwfTestAppConstants.getAllMeters)
    Call<List<MetersModel>> getMeters(@Header("Authorization") String token);
}
