package com.chrismour.gwftestapp.httpclient;

import android.content.Context;

import com.chrismour.gwftestapp.BuildConfig;
import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

//API Client for calls that need authorization token
public class APIClient {
    private static Retrofit retrofit = null;
    private  static TokenServiceHolder tokenServiceHolder;

    public static Retrofit getClient(Context mContext) {

        //Initialize TokenService - Singleton to hold the service
        tokenServiceHolder = TokenServiceHolder.getInstance();

        //Build OkHttpClient
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //Show logs only in debyc
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            httpClient.addInterceptor(logging);
        }

        httpClient
                .addInterceptor(new NetworkConnectionInterceptor(mContext)) //check if internet is available
                .authenticator(new TokenAuthenticator(mContext, tokenServiceHolder)) //refresh token authenticator
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        //Use gson for serializing the responses
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(gwfTestAppConstants.baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        return retrofit;
    }
}






