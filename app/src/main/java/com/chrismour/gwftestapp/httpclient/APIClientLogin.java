package com.chrismour.gwftestapp.httpclient;

import android.content.Context;

import com.chrismour.gwftestapp.BuildConfig;
import com.chrismour.gwftestapp.models.constants.gwfTestAppConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

//API client for login - Different because it does not need to handle authorization tokens
public class APIClientLogin {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(Context mContext) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            httpClient.addInterceptor(logging);
        }

        httpClient
                .addInterceptor(new NetworkConnectionInterceptor(mContext))
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(gwfTestAppConstants.baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();


        return retrofit;
    }
}






